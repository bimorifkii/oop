<?php

require_once("hewan.php");
require_once("Frog.php & Ape.php");

$sheep = new animal("shaun");

echo "Name : ". $sheep->name . "<br>"; 
echo "Legs : ". $sheep->legs . "<br>"; 
echo "Cold Blooded : ". $sheep->cold_blooded . "<br>";

$sungokong = new ape("kera sakti");

echo "Name : ". $sungokong->name . "<br>"; 
echo "Legs : ". $sungokong->legs . "<br>"; 
echo "Cold Blooded : ". $sungokong->cold_blooded . "<br>";
echo "Yell : " ;
$sungokong->yell();
echo "<br>";

$kodok = new frog("buduk");
echo "Name : ". $kodok->name . "<br>"; 
echo "Legs : ". $kodok->legs . "<br>"; 
echo "Cold Blooded : ". $kodok->cold_blooded . "<br>";
echo "Jump : " ;
$kodok->Jump();


?>